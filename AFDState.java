import java.util.*;

public class AFDState extends State {

    public ArrayList<State> contains;

    public AFDState(String id){
        super(id);
        this.contains = new ArrayList<State>();
    }

    public void addState(State state){
        this.contains.add(state);
    }

    public boolean isEqualTor(AFDState afd_state){
        if(this.contains.size() == afd_state.contains.size()){
            boolean flag = false;
            for(int i = 0; i < afd_state.contains.size(); i++){
                boolean tempflag = false;
                for(State state : this.contains){
                    if(state.equals(afd_state.contains.get(i))){
                        tempflag = true;
                        break;
                    }
                }
                if(!tempflag){
                    return false;
                }
            }
        }else{
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        String print = this.id + "=";

        if(this.contains.size() > 0){
            for(State state : this.contains){
                print += state.id + ",";
            }
            print = print.substring(0, print.length() -1);
        }else{
            print += "err";
        }

        return print;
    }
}
