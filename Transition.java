public class Transition {

    public String symbol;
    public State destination;

    public Transition (String symbol, State destination){
        this.symbol = symbol;
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o){
        return symbol.equals(((Transition) o).symbol) && this.destination == ((Transition) o).destination;
    }

    @Override
    public String toString(){
        return this.symbol + "->" + this.destination;
    }
}
