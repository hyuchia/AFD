 import java.util.*;

public class State {

    public String id;
    public boolean isInitial;
    public boolean isFinal;
    public ArrayList<Transition> transitions;
    public ArrayList<State> states;

    public State(String id){
        this.id = id;
        transitions = new ArrayList<Transition>();
        states = new ArrayList<State>();
    }

    public void addTransition(String symbol, String state){
        transitions.add(new Transition(symbol, state));
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof String){
            return id.equals((String) o);
        }else if(o instanceof State){
            return id.equals(((State) o).id);
        }
        return false;
    }

    @Override
    public String toString(){
        if(states.size() > 0){
            return id + "=" + states;
        }else{
            return id;   
        }
    }























    public String transition (String symbol) {
        if(transitions.contains(symbol)){
            return transitions.get(transitions.indexOf(symbol)).destination;
        }
        return null;
    }
    
    public boolean matches(State dfa_state){
        if(this.states.size() == dfa_state.states.size()){
            boolean flag = false;
            for(int i = 0; i < dfa_state.states.size(); i++){
                boolean tempflag = false;
                for(State state : this.states){
                    if(state.equals(dfa_state.states.get(i))){
                        tempflag = true;
                        break;
                    }
                }
                if(!tempflag){
                    return false;
                }
            }
        }else{
            return false;
        }
        return true;
    }

    public ArrayList<String> getAllDestinationsFrom(String symbol){
        ArrayList<String> destinations = new ArrayList<String>();
        for(Transition t : transitions){
            if(t.symbol.equals(symbol)){
                destinations.add(t.destination);
            }
        }
        return destinations;
    }
    
        public void addState(State state){
        states.add(state);
    }
}