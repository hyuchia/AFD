public class Transition {
    
    String symbol;
    String destination;
    
    public Transition(String symbol, String destination){
        this.symbol = symbol;
        this.destination = destination;
    }
    
    @Override
    public String toString(){
        return symbol + "->" + destination;
    }
    
}