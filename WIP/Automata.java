public interface Automata {
    
    
    public void addTransition(String origin, String symbol, String destination);
    public void addSymbol(String symbol);
    
    public void addState(String id);
    public void setInitial(String id);
    public void setFinal(String id);
    
    public void addState(State state);
    public void setInitial(State state);
    public void setFinal(State state);
    
    public State get(String id);
    public boolean hasSymbol(String Symbol);
    
    public State get(State state);
    
}