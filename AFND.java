import java.util.*;
import java.io.*;

public class AFND {

    public ArrayList<State> states;
    public ArrayList<String> alphabet;
    public ArrayList<State> finalStates;
    public State initialState;

    public AFND(){
        // Initialize Lists
        this.states = new ArrayList<State>();
        this.alphabet = new ArrayList<String>();
        this.finalStates = new ArrayList<State>();

        // Read File and set given information
        this.readFile();

        // Add Lambda to the alphabet
        this.alphabet.add("lmd");
    }

    // Finds and returns a State given it's id from the states List
    private State find(String id){
        return states.get(states.indexOf(new State(id)));
    }

    // Reads File and sets given information
    public void readFile(){
        try{
            BufferedReader br = new BufferedReader(new FileReader("data.txt"));
            String line = br.readLine();

            int lineNumber = 0;
            while (line != null) {

                switch(lineNumber){
                    // Read all States and add them to the list
                    case 0:
                        String[] states = line.split(",");
                        for(int i = 0; i < states.length; i++){
                            this.states.add(new State(states[i]));
                        }
                        //System.out.println(this.states);
                        break;

                    // Read all the symbols for the alphabet and add them
                    case 1:
                        String[] symbols = line.split(",");
                        for(int i = 0; i < symbols.length; i++){
                            this.alphabet.add(symbols[i]);
                        }
                        //System.out.println(this.alphabet);
                        break;

                    // Set the Initial State
                    case 2:
                        this.initialState = this.find(line);
                        this.find(line).isInitial = true;
                        //System.out.println(this.initialState);
                        break;

                    // Set the list of Final States
                    case 3:
                        String[] finalStates = line.split(",");
                        for(int i = 0; i < finalStates.length; i++){
                            this.find(finalStates[i]).isFinal = true;
                            this.finalStates.add(this.find(finalStates[i]));
                        }
                        //System.out.println(this.finalStates);
                        break;

                    // Set all the transitions
                    default:
                        if (line.indexOf("->") > -1) {
                            String[] parts = line.split("->");
                            String[] left = parts[0].split(",");
                            String[] right = parts[1].split(",");

                            for(int i = 0; i < right.length; i++){
                                if(this.alphabet.contains(left[1])){
                                    this.find(left[0]).addTransition(left[1], this.find(right[i]));
                                }
                            }

                        }
                        break;
                }
                line = br.readLine();
                lineNumber++;
            }
            br.close();
        }catch(Exception e){
            System.out.println("There was an error while reading the file\n" + e);
        }
    }

    @Override
    public String toString(){
        String afnd = "===============";
        afnd += "\nAFND\n";
        afnd += "===============";
        afnd += "\nSTATES:\n";
        afnd += this.states + " ";
        afnd += "\nALPHABET:\n";
        afnd += this.alphabet + " ";
        afnd += "\nINITIAL STATE:\n";
        afnd += this.initialState.id;
        afnd += "\nFINAL STATES:\n";
        afnd += this.finalStates + " ";
        afnd += "\nTRANSITIONS:\n";
        for(State state : this.states){
            for(Transition t : state.transitions){
                afnd += state.id + ' ' + t.toString();
                afnd += "\n";
            }
        }
        return afnd;
    }
}
