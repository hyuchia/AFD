import java.util.*;

public class State {

    public String id;
    public boolean isInitial;
    public boolean isFinal;
    public ArrayList<Transition> transitions;

    public State(String id){
        this.id = id;
        this.transitions = new ArrayList<Transition>();
    }

    public void addTransition(String symbol, State state){
        this.transitions.add(new Transition(symbol, state));
    }

    public State transition (String symbol) {
        if(this.transitions.contains(symbol)){
            return this.transitions.get(this.transitions.indexOf(symbol)).destination;
        }
        return null;
    }

    public ArrayList<State> getAllDestinationsFrom(String symbol){
        ArrayList<State> destinations = new ArrayList<State>();
        for(Transition t : transitions){
            if(t.symbol.equals(symbol)){
                destinations.add(t.destination);
            }
        }
        return destinations;
    }

    @Override
    public boolean equals(Object o) {
        State temp = (State) o;
        System.out.println(temp.id);
        return this.id.equals(temp.id);
    }

    @Override
    public String toString(){
        return this.id;
    }
}
