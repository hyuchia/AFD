import java.util.*;
import java.io.*;

public class AFD {

    private ArrayList<AFDState> states;
    private ArrayList<String> alphabet;
    private ArrayList<AFDState> finalStates;
    private AFDState initialState;


    public AFD(){
        this.states = new ArrayList<AFDState>();
        this.alphabet = new ArrayList<String>();
        this.finalStates = new ArrayList<AFDState>();
    }

    // Transform given AFND to AFD
    public void transformFrom(AFND afnd){
        // Same alphabet
        this.alphabet = afnd.alphabet;
        this.alphabet.remove("lmd");
        // STEP 1
        // Create initial state wich contains AFND's initial state
        AFDState initial = new AFDState("Q0");
        initial.addState(afnd.initialState);
        this.states.add(initial);
        this.initialState = initial;

        // STEP 2
        // lmd-closure
        getLambdaC(afnd);
    }

    private void getLambdaC(AFND afnd){
        // Get list of AFND states
        ArrayList<State> afnd_states = afnd.states;
        // Set default lambda transition to every state
        for(State state : afnd_states){
            state.addTransition("lmd", state);
        }
        // STEP 4
        // Transition table
        getStates(afnd);
    }

    private void getStates(AFND afnd){
        // Get list of AFND states
        int count = 0;
        // Loop through every AFD state
        while(count < this.states.size() ){
            AFDState afd_state = this.states.get(count);
            System.out.println("\nDEBUG: afd_state -> " + afd_state.id);
            // Get every transition per letter from each state in ADF state
            for(String letter : this.alphabet){
                System.out.println("DEBUG: Current letter -> " + letter);
                // Placeholder for destination states
                ArrayList<State> l_destinations = new ArrayList<State>();
                // Decompose AFD state
                for(State state : afd_state.contains){
                    // Get state destinations
                    ArrayList<State> s_destinations = state.getAllDestinationsFrom(letter);
                    System.out.println("DEBUG: All destinations from " + afd_state.id + " with letter " + letter);
                    for(State d_s : s_destinations){
                        System.out.println("D: " + d_s.id);
                    }
                    // Loop through every destination
                    for(State t_state : s_destinations){
                        // If destination is not already on l_destinations
                        if(!l_destinations.contains(t_state)){
                            l_destinations.add(t_state);
                        }

                        // Get lambda destination
                        ArrayList<State> lmd_destination = t_state.getAllDestinationsFrom("lmd");
                        System.out.println("DEBUG: All lmd destinations");

                        for(State l_state : lmd_destination){
                            // If destination is not already on l_destinations
                            if(!l_destinations.contains(l_state)){
                                l_destinations.add(l_state);
                            }
                        }
                        for(State d_s : lmd_destination){
                            System.out.println("D: " + d_s.id);
                        }
                    }
                }
                // Create temporal AFD state for reference
                AFDState temp_afd = new AFDState("Q" + (this.states.size()));
                System.out.println("DEBUG: Placeholder AFD state -> " + temp_afd.id);
                // Populate AFD state with every destination state of letter
                if(l_destinations.size() != 0){
                    for(State n_state : l_destinations){
                        temp_afd.addState(n_state);
                    }
                }else{
                    temp_afd.addState(new State("err"));
                }
                System.out.println("DEBUG: Placeholder states");
                for(State d_s : temp_afd.contains){
                    System.out.println("D: " + d_s.id);
                }
                // Check if state already exists
                boolean exists = false;
                int existing_position = -1;
                for(AFDState e_state : this.states){
                    existing_position++;
                    System.out.println("DEBUG: COMPARING " + e_state.id + " with " + temp_afd.id);
                    if(e_state.isEqualTor(temp_afd) == true){
                        System.out.println("DEBUG: MATCH FOUND AT -> " + existing_position);
                        exists = true;
                        break;
                    }
                }
                if(!exists){
                    System.out.println("DEBUG: Placeholder does not exist yet");
                    // Add new AFD state
                    this.states.add(temp_afd);
                    System.out.println("DEBUG: Add placeholder state");
                    // Assign transition to current AFD to existing one
                    afd_state.addTransition(letter, temp_afd);
                    System.out.println("DEBUG: Add transition from " + afd_state.id + " with " + letter + " to " + temp_afd.id);
                }else{
                    System.out.println("DEBUG: Placeholder already exists");
                    System.out.println("DEBUG: Position in AFD states -> " + existing_position);
                    // Get AFD state
                    AFDState existing_state = this.states.get(existing_position);
                    System.out.println("D: " + existing_state.id);
                    // Assign transition to current AFD to existing one
                    afd_state.addTransition(letter, existing_state);
                    System.out.println("DEBUG: Add transition from " + afd_state.id + " with " + letter + " to " + existing_state.id);
                    // Reasign modified AFD state to states
                    this.states.set(existing_position, existing_state);
                }
            }
            count++;
        }
        this.getFinalStates(afnd);
    }

    private void getFinalStates(AFND afnd){
        ArrayList<State> afnd_finals = afnd.finalStates;
        for(AFDState afd_state : this.states){
            for(State state : afnd_finals){
                if(afd_state.contains.contains(state)){
                    this.finalStates.add(afd_state);
                }
            }
        }
        this.writeFile();
    }

    public void setInitialState (State state) {
        this.initialState = new AFDState(state.id.toUpperCase());
        this.states.add(this.initialState);
    }

    public void setAlphabet(ArrayList<String> alphabet){
        this.alphabet = alphabet;
        this.alphabet.remove("lmd");
    }

    private AFDState find(String id){
        return states.get(states.indexOf(new AFDState(id)));
    }

    public void createState(String id){
        this.states.add(new AFDState(id));
    }

    private void writeFile(){
        try{
            BufferedWriter br = new BufferedWriter(new FileWriter("result.txt"));
            String print = "";

            String transitions = "";
            for(AFDState state : this.states){
                print += state + ", ";
                for(Transition transition : state.transitions){
                    transitions += state.id + "," + transition.symbol + "->" + transition.destination.id + "\n";
                }
            }
            print = print.substring(0, print.length() -2);
            print += "\n" + this.alphabet.toString().replace(" ", "").replace("[", "").replace("]", "") + "\n";

            print += this.initialState.id + "\n";

            for(AFDState state : this.finalStates){
                print += state.id + ",";
            }
            print = print.substring(0, print.length() -1);

            print += "\n" + transitions;

            br.write(print);
            br.close();

        }catch(Exception e){
            System.out.println("There was an error while writing the file\n" + e);
        }
    }

    @Override
    public String toString(){
        String print = "\n===============\n";
        print += "ADF";
        print +=  "\n===============\n";

        print += "STATES:\n";
        String transitions = "";
        for(AFDState state : this.states){
            print += state + " ";
            for(Transition transition : state.transitions){
                transitions += state.id + "," + transition.symbol + "->" + transition.destination.id + "\n";
            }
        }

        print += "\nALPHABET:\n";
        print += this.alphabet.toString().replace(" ", "").replace("[", "").replace("]", "") + "\n";

        print += "FINAL STATES:\n";
        print += this.initialState.id + "\n";
        for(AFDState state : this.finalStates){
            print += state.id + ",";
        }
        print = print.substring(0, print.length() -1);

        print += "\nTRANSITIONS:\n";
        print += transitions;
        return print;
    }
}
